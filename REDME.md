# Quiz "Who Wants to Be a Millionaire?" - documentation #
Project's author:
<br>Batko Dominik
<br>Cich Bartłomiej
<br>Markowicz Wiktor

This file contains the quiz instructions, information about the tools used in the project and a table with the division of work.

## User manual ##

1. The user of the game, after starting the application, sees questions and 4 possible answers to choose from.
2. After each good answer, the user moves on to the next question, thus increasing his winnings.
3. If a player answers the question incorrectly, he is out of the game!
4. After giving the 15th correct answer, the user wins.


## Screenshots from the application ##
<br/>
1. To start the game, the user must click the "Start" button. <br/>
![](https://bitbucket.org/d_batko/project_07_34269/raw/9f7da4108e0eececd936a880a9a4b94b1b8393b7/img/first.png )

<br/><br/>
2. First question. The overall appearance of the application. <br/>
![](https://bitbucket.org/d_batko/project_07_34269/raw/9f7da4108e0eececd936a880a9a4b94b1b8393b7/img/second.png )

<br/><br/>
3. Information about a good answer along with the amount of the win. <br/>
Click "Kolejne pytanie" button to go to the next question. <br/>
![](https://bitbucket.org/d_batko/project_07_34269/raw/9f7da4108e0eececd936a880a9a4b94b1b8393b7/img/third.png )

<br/><br/>
4. Information about giving the wrong answer. <br/>
Showing the correct answer and winning. <br/>
Allowing you to start game once again. <br/>
![](https://bitbucket.org/d_batko/project_07_34269/raw/9f7da4108e0eececd936a880a9a4b94b1b8393b7/img/fourth.png )

<br/><br/>
5. Lifelines.<br/>

* 50:50.<br/>
![](https://bitbucket.org/d_batko/project_07_34269/raw/9f7da4108e0eececd936a880a9a4b94b1b8393b7/img/fifth.png )
<br/>
* Phone-a-Friend.<br/>
![](https://bitbucket.org/d_batko/project_07_34269/raw/9f7da4108e0eececd936a880a9a4b94b1b8393b7/img/sixth.png)
<br/>
* Audience.<br/>
![](https://bitbucket.org/d_batko/project_07_34269/raw/9f7da4108e0eececd936a880a9a4b94b1b8393b7/img/seventh.png)
<br/><br/>

6. Guarantee Points. <br/>

* $1 000 <br/>
* $32 000 <br/>
![](https://bitbucket.org/d_batko/project_07_34269/raw/9f7da4108e0eececd936a880a9a4b94b1b8393b7/img/eighth.png )

<br/><br/>
7. The screen after giving the correct answer to the last 15th question. <br/>
![](https://bitbucket.org/d_batko/project_07_34269/raw/9f7da4108e0eececd936a880a9a4b94b1b8393b7/img/ninth.png )


## Software development tools ##

* Programming language: Java.
* Text editor: IntelliJ IDEA Community Edition 2020.2.4.
* Technology: JavaFX.
* Tool automating software development: Maven.

## Division of work ##
Batko Dominik | Cich Bartłomiej | Markowicz Wiktor |
 --- | --- | --- |
application algorithm | GUI | lifelines | 
code | questions | questions | 
- | graphical view of stages and awards | REDME file |

EN: Above are only examples of tasks that were performed by individual people, because each person in the group was involved in the same way in the implementation of the project. <br/>
PL: Powyżej zapisano tylko przykładowe zadania, które były wykonane przez poszczególne osoby, ponieważ każda z osób w grupie była zaangażowana w jednakowy sposób w realizację projektu.