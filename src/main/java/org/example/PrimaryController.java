package org.example;

import java.io.IOException;
import java.util.Random;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

public class PrimaryController {
    Game x = new Game();
    int fiftyCheck = 0;
    String[] money = {"$100","$200","$300","$500","$1,000","$2,000","$4,000","$8,000","$16,000","$32,000","$64,000","$125,000","$250,000","$500,000","$1,000,000"};
    @FXML
    Button b1, b2, b3, b4, start, next, help50, helpFriend, helpAudience;
    @FXML
    Label question;
    @FXML
    VBox table;
    @FXML
    private void start() throws IOException {
        this.fiftyCheck = 0;
        table.getStyleClass().remove("q"+x.getI());
        table.getStyleClass().add("q1");
        start.setVisible(false);
        help50.setDisable(false);
        help50.getStyleClass().remove("noFiftyFifty");
        helpFriend.setDisable(false);
        helpFriend.getStyleClass().remove("noAskFriend");
        helpAudience.setDisable(false);
        helpAudience.getStyleClass().remove("noAudience");
        x.setI();
        textSet();
    }
    @FXML
    private void ans1() throws IOException{
        hide();
        if(x.test(b1.getText()) == 1) {
            correct();
        }
        else if(x.test(b1.getText()) == 2){
            victory();
        }
        else {
            wrong();
        }

    }
    @FXML
    private void ans2() throws IOException{
        hide();
        if(x.test(b2.getText()) == 1) {
            correct();
        }
        else if(x.test(b2.getText()) == 2){
            victory();
        }
        else {
            wrong();
        }
    }
    @FXML
    private void ans3() throws IOException{
        hide();
        if(x.test(b3.getText()) == 1) {
            correct();
        }
        else if(x.test(b3.getText()) == 2){
            victory();
        }
        else {
            wrong();
        }
    }
    @FXML
    private void ans4() throws IOException{
        hide();
        if(x.test(b4.getText()) == 1) {
            correct();
        }
        else if(x.test(b4.getText()) == 2){
            victory();
        }
        else {
            wrong();
        }
    }
    @FXML
    private void next() throws IOException{
        this.fiftyCheck = 0;
        next.setVisible(false);
        table.getStyleClass().remove("q"+(x.getI()-1));
        table.getStyleClass().add("q"+x.getI());
        textSet();
    }
    @FXML
    private void hide() throws IOException{ //EN: Hiding all answers buttons and lifelines / PL: Ukrycie wszystkich przycisków odpowiedzi i kół ratunkowych
        b1.setVisible(false);
        b2.setVisible(false);
        b3.setVisible(false);
        b4.setVisible(false);
        help50.setVisible(false);
        helpFriend.setVisible(false);
        helpAudience.setVisible(false);
        table.setVisible(false);
    }
    @FXML
    private void textSet() throws IOException{ //EN: Setting buttons visible, shuffle answers and assignment them to answers buttons and assignment question to the question label / PL: Ustawienie przycisków widzialnych wraz z pobraniem pytania i odpowiedzi oraz losowe przypisanie ich do przycisków
        b1.setVisible(true);
        b2.setVisible(true);
        b3.setVisible(true);
        b4.setVisible(true);
        help50.setVisible(true);
        helpFriend.setVisible(true);
        helpAudience.setVisible(true);
        table.setVisible(true);
        String[] y = x.start();
        int[] z = x.shuff();
        question.setText(y[0]);
        b1.setText(y[z[0]]);
        b2.setText(y[z[1]]);
        b3.setText(y[z[2]]);
        b4.setText(y[z[3]]);

    }
    @FXML
    private void fiftyFifty() throws IOException{ //EN: Lifeline fifty fifty / PL: Koło ratunkowe 50/50
        this.fiftyCheck = 1;
        help50.setDisable(true);
        help50.getStyleClass().add("noFiftyFifty");
        int w = 0;
        int o;
        int[] z = x.shuff();
        for(int j = 0; j < 4; j++) {
            o = z[j];
            switch (o) {
                case 1:
                    if (x.test_lifelines(b1.getText()) == 0 && w < 2) {
                        b1.setVisible(false);
                        w++;
                    }
                    break;
                case 2:
                    if (x.test_lifelines(b2.getText()) == 0 && w < 2) {
                        b2.setVisible(false);
                        w++;
                    }
                    break;
                case 3:
                    if (x.test_lifelines(b3.getText()) == 0 && w < 2) {
                        b3.setVisible(false);
                        w++;
                    }
                    break;
                case 4:
                    if (x.test_lifelines(b4.getText()) == 0 && w < 2) {
                        b4.setVisible(false);
                        w++;
                    }
                    break;
            }
        }
    }
    @FXML
    private void friendAsk() throws IOException{ //EN: Lifeline phone to friend / PL: Koło ratunkowe telefon do przyjaciela
        int w = x.getI();
        Friends a = new Friends();
        helpFriend.setDisable(true);
        helpFriend.getStyleClass().add("noAskFriend");
        Alert a1;
        if(w < 6) { //EN: Always correct answer during the first five questions / PL: Zawsze poprawna odpowiedź do piątego włącznie pytania
            a1 = new Alert(Alert.AlertType.NONE,
                    a.start("osoby") + x.getCorr(), ButtonType.OK);
        }
        else{ //EN: Draw one random answer / PL: Losowanie jednej odpowiedzi
            Random r = new Random();
            String ans = "";
            if(this.fiftyCheck==1){
                int ran = r.nextInt(2)+1;
                int v = 1;
                if(b1.isVisible()){
                    if(ran == 1){
                        ans = b1.getText();
                    }
                    else{
                        v++;
                    }
                }
                if(b2.isVisible()){
                    if(ran == 1 || v == 2){
                        ans = b2.getText();
                    }
                    else{
                        v++;
                    }
                }
                if(b3.isVisible()){
                    if(ran == 1 || v == 2){
                        ans = b3.getText();
                    }
                    else{
                        v++;
                    }
                }
                if(b4.isVisible()){
                    if(ran == 2){
                        ans = b4.getText();
                    }
                }
            }

            else{
                int ran = r.nextInt(4)+1;

                switch(ran) {
                    case 1:
                        ans = b1.getText();
                        break;
                    case 2:
                        ans = b2.getText();
                        break;
                    case 3:
                        ans = b3.getText();
                        break;
                    case 4:
                        ans = b4.getText();
                        break;
                }
            }
            a1 = new Alert(Alert.AlertType.NONE,
                    a.start("osoby") + ans, ButtonType.OK);

        }
        a1.setTitle("Rozmowa z przyjacielem");
        a1.show();
    }
    @FXML
    private void audience() throws IOException{ //EN: Lifeline question to audience / PL: Koło ratunkowe pytanie do publiczności
        helpAudience.setDisable(true);
        helpAudience.getStyleClass().add("noAudience");
        Random r = new Random();
        int w = 0;
        int[] ran = new int[4];
        String[] ans = new String[4];
        int correct = r.nextInt(51)+50;
        int j = 100-correct;
        if(b1.isVisible()) {
            if (x.test_lifelines(b1.getText()) == 1) {
                ran[w] = correct;
            } else {
                if(b2.isVisible() && b3.isVisible()){
                    int wrong = r.nextInt(j + 1);
                    ran[w] = wrong;
                    j -= wrong;
                }
                else {
                    ran[w] = j;
                }
            }
            ans[w] = b1.getText();
            w++;
        }
        if(b2.isVisible()) {
            if (x.test_lifelines(b2.getText()) == 1) {
                ran[w] = correct;
            } else {
                if(b1.isVisible() && b3.isVisible()){
                    int wrong = r.nextInt(j + 1);
                    ran[w] = wrong;
                    j -= wrong;
                }
                else {
                    ran[w] = j;
                }
            }
            ans[w] = b2.getText();
            w++;
        }
        if(b3.isVisible()) {
            if (x.test_lifelines(b3.getText()) == 1) {
                ran[w] = correct;
            } else {
                if(b1.isVisible() && b2.isVisible()){
                    int wrong = r.nextInt(j + 1);
                    ran[w] = wrong;
                    j -= wrong;
                }
                else {
                    ran[w] = j;
                }
            }
            ans[w] = b3.getText();
            w++;
        }
        if(b4.isVisible()) {
            if (x.test_lifelines(b4.getText()) == 1) {
                ran[w] = correct;
            } else {
                ran[w] = j;
            }
            ans[w] = b4.getText();
        }
        Alert a1;

        if(w == 1 || w == 2){
            a1 = new Alert(Alert.AlertType.NONE,
                    ans[0] + " " + ran[0] + "%\n" + ans[1] + " " + ran[1] + "%", ButtonType.OK);
        }
        else {
            a1 = new Alert(Alert.AlertType.NONE,
                    b1.getText() + " " + ran[0] + "%\n" + b2.getText() + " " + ran[1] + "%\n" + b3.getText() + " " + ran[2] + "%\n" + b4.getText() + " " + ran[3] + "%\n", ButtonType.OK);
        }
        a1.setTitle("Odpowiedzi publiczności");
        a1.show();

    }
    @FXML
    private void wrong() throws IOException{
        if(x.getI()>5 && x.getI()<11) {
            question.setText("Przegrana! Poprawna odpowiedź to: " + x.getCorr() + "\nWygrałeś "+money[4]);
        }
        else if(x.getI()>10) {
            question.setText("Przegrana! Poprawna odpowiedź to: " + x.getCorr() + "\nWygrałeś "+money[9]);
        }
        else{
            question.setText("Przegrana! Poprawna odpowiedź to: " + x.getCorr() + "\nNiestety nic nie wygrałeś");
        }
        start.setText("Zagraj jeszcze raz");
        start.setVisible(true);
    }

    @FXML
    private void correct() throws IOException{
        question.setText("Dobra odpowiedź, masz na ten moment "+money[x.getI()-2]);
        next.setVisible(true);
    }
    @FXML
    private void victory() throws IOException{
        question.setText("Gratulacje, wygrałeś główną nagrodę "+money[14]);
        start.setText("Zagraj jeszcze raz");
        start.setVisible(true);
    }
}
