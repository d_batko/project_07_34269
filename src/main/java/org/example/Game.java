package org.example;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.StringTokenizer;

public class Game {

    String corr;
    int i = 1;

    String[] start() throws IOException {
        ArrayList<String> ar = new ArrayList<String>();
        FileInputStream csvFile = new FileInputStream("src\\"+i+".csv");
        InputStreamReader zxc = new InputStreamReader(csvFile, StandardCharsets.UTF_8);
        BufferedReader br = new BufferedReader(zxc);
        String line = "";
        StringTokenizer st = null;
        int lineNumber = 0;
        int tokenNumber = 0;
        while ((line = br.readLine()) != null) {
            lineNumber++;
            st = new StringTokenizer(line, ";");
            while (st.hasMoreTokens()) {
                tokenNumber++;
                String sd=st.nextToken() + "  ";
                if(sd!=null){
                    ar.add(sd);
                }
            }
            tokenNumber = 0;
        }
        int los = rand();
        this.corr = ar.get(8+7*los+5);
        String[] odp = new String[5];
        odp[0] = ar.get(8+7*los);
        odp[1] = ar.get(8+7*los+1);
        odp[2] = ar.get(8+7*los+2);
        odp[3] = ar.get(8+7*los+3);
        odp[4] = ar.get(8+7*los+4);
        return odp;
    }

    int test(String answer) throws IOException{
        if (this.corr.equals(answer)){
            if(i < 15) {
                i++;
                return 1;
            }
            return 2;
        }
        return 0;
    }

    int test_lifelines(String answer) throws IOException{
        if (this.corr.equals(answer)){
            if(i < 15) {
                return 1;
            }
        }
        return 0;
    }

    void setI() throws IOException{
        i = 1;
    }

    int rand(){
        Random liczba = new Random();
        int los = liczba.nextInt(10);
        return los;
    }

    int[] shuff(){ //shuffle questions
        ArrayList<Integer> numbers = new ArrayList<>();
        for(int i = 1; i < 5; ++i) {
            numbers.add(i);
        }
        Collections.shuffle(numbers);
        int[] num = new int[4];
        num[0] = numbers.get(0);
        num[1] = numbers.get(1);
        num[2] = numbers.get(2);
        num[3] = numbers.get(3);
        return num;
    }

    public int getI() {
        return i;
    }

    public String getCorr() {
        return corr;
    }
}
