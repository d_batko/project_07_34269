package org.example;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Random;
import java.util.StringTokenizer;

public class Friends {
    String start(String i) throws IOException {
        ArrayList<String> ar = new ArrayList<String>();
        FileInputStream csvFile = new FileInputStream("src\\"+i+".csv");
        InputStreamReader zxc = new InputStreamReader(csvFile, StandardCharsets.UTF_8);
        BufferedReader br = new BufferedReader(zxc);
        String line = "";
        StringTokenizer st = null;
        int lineNumber = 0;
        int tokenNumber = 0;
        while ((line = br.readLine()) != null) {
            lineNumber++;
            st = new StringTokenizer(line, ";");
            while (st.hasMoreTokens()) {
                tokenNumber++;
                String sd=st.nextToken() + "  ";
                if(sd!=null){
                    ar.add(sd);
                }
            }
            tokenNumber = 0;
        }
        int los = rand();
        return ar.get(los);
    }

    int rand(){
        Random liczba = new Random();
        int los = liczba.nextInt(8);
        return los;
    }
}
